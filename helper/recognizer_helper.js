var enGreetings = /(hi|hey|hey there|hello|hello hello|what up|what\'s up|whatup|salute|morning|good morning|how are you|how r u|good day|whats up|welcome|mar7aba|marhab|hala|ahln|evening|afternoon|good evening|good afternoon)\b/;

var arGreetingsArr = [
    'السلام عليكم ورحمه الله وبركاته', 'هاي', 'هلا', 'مرحبا', 'السلام عليكم', 'صباح الخير', 'مساء الخير', 'كيفك', 'كيف الحال', 'ايش الاخبار', 'هاللو', 'بعد التحيه', 'هالو', 'السلام عليك', 'ازيك', 'يافندم', 'سلام عليك', 'اهلا', 'حياك الله', 'مساكم الله بالخير', 'صبحكم', 'الله بالخير','ايش لونك','ايش اخبارك','هلا','اهلا و سهلا'
];
var arGreetings = new RegExp('('+ arGreetingsArr.join("|")+')'+ '(.*)')

module.exports = {
    welcomeRecognizer : function(text){
        return new Promise((resolve) => {
            text = text
                .replace(/[!?,.\/\\\[\]\{\}\(\)]/g, '')
                .trim()
                .toLowerCase();
            text = text.replace(/[أإآ]/g, 'ا').replace('ى', 'ي').replace('ة', 'ه').replace('ـ', '');    
            let englishGreeting = enGreetings.test(text);
            let arabicGreeting = arGreetings.test(text);
            let recognized = {
                recognized: false
            };
            if (englishGreeting) {
                recognized = {
                    recognized: true,
                    lang: 'en'
                }
            }else if(arabicGreeting){
                recognized = {
                    recognized: true,
                    lang: 'ar'
                }
            }
            resolve(recognized);
        });
    }
}