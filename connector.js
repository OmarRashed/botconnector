
var io = require('socket.io-client');
var async = require('async');
//var Swagger = require('swagger-client');
var request = require('request');
var rp = require('request-promise');
var conversations = {};
redisHelper = require('./helper/redis_helper');
var PREFIX = process.env.PREFIX ? process.env.PREFIX : 'care';
var APIKEY = process.env.APIKEY ? process.env.APIKEY : '15ac4851f6a69b789c769fe62b73fab5e911a7f2';
var directLineSecret = process.env.DIRECT_LINE_SECRET ? process.env.DIRECT_LINE_SECRET : 'Xk_UWTDxoSQ.4X6nWc82En4Auge0Z0hSuB6Nim0xwhr9WYZxjwv_pTw';
//var directLineSpecUrl = 'https://bubbles.cc/js/tmp/directline-3.0.json'; //'https://docs.botframework.com/en-us/restapi/directline3/swagger.json';var sessionConfigCaching;
//ToDo:
// var PREFIX = 'linkdevfbbottest';
// var APIKEY = '67de818a43419981c562f463e1f8675988876ac4';
// var directLineSecret = 'vNhFwbWRIhM.OXYEipc5zuB3ClUhGvA6QV8DebF0lYG5NHGfqTKIMpk';
//var directLineSpecUrl = 'https://bubbles.cc/js/tmp/directline-3.0.json'; //'https://docs.botframework.com/en-us/restapi/directline3/swagger.json';var sessionConfigCaching;


const https = require('https');
const HttpsAgent = require('agentkeepalive').HttpsAgent;
const keepaliveAgent = new HttpsAgent({
    maxSockets: 20,
    maxFreeSockets: 10,
    timeout: 60000,
    freeSocketTimeout: 30000, // free socket keepalive for 30 seconds
});
var socketioClient;
var PROTOCOL;
var HOST;
var DomainName;
var msglimitPerPackage;
var msglimitPerDay;
var currentMsgsCount;
var dayMsgsCount = 0;
/*Disabled 
 const HttpsAgent = require('agentkeepalive').HttpsAgent;
 const nodeProcesses = process.env.NodeProcesses || 2
 const maxSockets = 20 / nodeProcesses
 const keepaliveAgent = new HttpsAgent({maxSockets});
 */

switch (process.env.NODE_ENV) {
    case 'production':
        PROTOCOL = 'https';
        DomainName = HOST = 'bubbles.cc';
        SOCKETPORT = '443';
        PORT = '443';
        break;
    case 'dev_production':
        PROTOCOL = 'http';
        HOST = 'localhost';
        DomainName = 'staging.adsia.com';
        SOCKETPORT = process.env.PORT? process.env.PORT : '5050';
        PORT = '9001';
        break;
    default:
        PROTOCOL = 'https';
        // HOST = 'localhost';
        HOST = 'staging.adsia.com';
        SOCKETPORT = '443';
        PORT = '433';
        APIKEY = '15ac4851f6a69b789c769fe62b73fab5e911a7f2';
        PREFIX = 'bm-hrbot'
}

var asyncLogging = require('./hooks/asyncLogging');
var socketConnectionHandler = {};
var roomsArr = [];

process.on('asyncLogging', asyncLogging);
process.on('uncaughtException', function (err) {
    process.emit('asyncLogging', {type: 'uncaughtException', parameter: err.stack, prefix: PREFIX});
    var x = setTimeout(function () {
        process.exit(1);
    }, 1000 * 7);
});

makeSocketConnection();

// requestSessionConfig(function (sessionConfig) {
// process.sessionConfig = sessionConfig;
// msglimitPerPackage = process.sessionConfig.msglimitPerPackage || 60;//10000;
// msglimitPerDay = msglimitPerPackage / 12;
// getCurrentMsgCount();
//     makeSocketConnection(sessionConfig);
// });

function requestSessionConfig(callback) {
    var options = {
        url: PROTOCOL + '://' + HOST + ':' + PORT + '/' + PREFIX + '/api/getSessionConfig',
        method: 'POST',
        headers: {
            'sdkKey': APIKEY
        }
    };
    request(options, function (error, response, body) {
        if (error) {
            process.emit('asyncLogging', {type: 'logging', prefix: PREFIX, method: 'BotClientData|request|error', parameter: error.stack, options: options, response: response, prefix: PREFIX}); //err.stack
            return callback(process.sessionConfig);
        }
        try {
            body = JSON.parse(body);
        } catch (e) {
            var err = e || {};
            process.emit('asyncLogging', {type: 'logging', prefix: PREFIX, method: 'req|JSON.parse err', parameter: 'SyntaxError: Unexpected token < in JSON | body:' + body, prefix: PREFIX});
            return callback(process.sessionConfig);
        }
        process.sessionConfig = body.data;
        msglimitPerPackage = body.data.msglimitPerPackage || 10000;
        msglimitPerDay = msglimitPerPackage / 30;
        return callback(body.data);
    });
}
function getCachedRedisSessionConfig(callback) {
    var serverSessionConfig = PREFIX + '_sessionConfig0';
    redisHelper.socketStore.get(serverSessionConfig, function (getKeyErr, sessionConfigCached) {
        // console.log('sessionConfigCached:', sessionConfigCached)
        if (!getKeyErr && sessionConfigCached !== null) {
            return callback(JSON.parse(sessionConfigCached))
        }
        return requestSessionConfig(callback);
    })
}

function makeSocketConnection(data) {
    var connectionOptions = {
        reconnect: false, 'force new connection': true, query: "prefix=" + PREFIX + "&botClient=true&botCode=971d784e-e259-512c-a1c0-5af9b7e92e3e"
    };
    console.log("connectionOptions:", PROTOCOL + '://' + HOST + ':' + SOCKETPORT + JSON.stringify(connectionOptions));
    socketioClient = io.connect(PROTOCOL + '://' + HOST + ':' + SOCKETPORT, connectionOptions); // 'https://192.168.120.107'
    handleSocketioClientEvents();
}


function handleSocketioClientEvents() {
    socketioClient.removeListener('error').on('error', function (err) {
        process.emit('asyncLogging', {type: 'logging', parameter: err.stack, prefix: PREFIX});
        socketioClient.socket.reconnect();
        socketConnectionHandler.reconnectAttempt = setInterval(function () {
            socketioClient.socket.reconnect();
        }, 1000 * 60 * 1);
    });
    socketioClient.removeListener('disconnect').on('disconnect', function () {
        process.emit('asyncLogging', {type: 'logging', parameter: 'socketioClient disconnect', prefix: PREFIX});
        socketioClient.socket.reconnect();
        socketConnectionHandler.reconnectAttempt = setInterval(function () {
            socketioClient.socket.reconnect();
        }, 1000 * 60 * 1);
    });
    socketioClient.removeListener('connect').on('connect', function () {
        console.log('socketioClient|connected');
        clearInterval(socketConnectionHandler.reconnectAttempt);
        socketConnectionHandler.reconnectAttempt = null;
        delete socketConnectionHandler.reconnectAttempt

        process.emit('asyncLogging', {type: 'logging', parameter: 'socket connected', prefix: PREFIX});
        if (Object.keys(conversations).length > 0) {
            handleBorReconnected();
        }
        if (process.env.NODE_ENV === 'localhost') {
            let onlineDeptsRequestData = {id: '5de515832d13e50cf8bc98b7', prefix: PREFIX};
            socketioClient.emit('getOnlineDepts', onlineDeptsRequestData, function (onlineDeptsResponseData) {
                // console.log('onlineDeptsResponseData:', require('util').inspect(onlineDeptsResponseData, {depth: null}));
                try {
                    var obj = typeof JSON.parse(onlineDeptsResponseData) === "object" ? JSON.parse(onlineDeptsResponseData) : null;
                } catch (err) {
                    // console.log('>>getOnlineDepts|err:', err);
                    return self.sendMessagesFromConsole({msg: 'null'});
                }
                // console.log(">>>>>onlineDeptsResponseData:", obj)
                self.sendMessagesFromConsole({msg: obj[0][0]._id});
            });
        }
    });
    socketioClient.removeListener('BotWelcome').on('BotWelcome', function (data) {
        process.emit('asyncLogging', {type: 'logging', parameter: 'BotWelcome: ' + require('util').inspect(data), prefix: PREFIX});
    });
    socketioClient.removeListener('startNewConversation').on('startNewConversation', function (data) {
        console.log('startNewConversation|data:', require('util').inspect(data));
        // console.log("Data: ",data)
        conversations[data.room] = new BubblesBot(data); //data

    });
    socketioClient.removeListener('message').on('message', function (messageData) {

        if (process.env.NODE_ENV === 'production' || (conversations[messageData.room].thirdPartyCustomers === 'twitter' || conversations[messageData.room].thirdPartyCustomers === 'whatsapp')) {
            conversations[messageData.room].emitOpTyping({room: messageData.room});
            conversations[messageData.room].sendMessagesFromConsole(messageData);
        } else {
            let recognizer = require('./helper/recognizer_helper');
            recognizer.welcomeRecognizer(messageData.msg).then((recognizer) => {
                if (recognizer.recognized) {
                    getCachedRedisSessionConfig((sessionConfig) => {
                        let welcomeMsg = recognizer.lang === 'en' ? sessionConfig.welcomeBot_msg : sessionConfig.welcomeBot_msgAr;
                        if (sessionConfig.enableDialogWithGreeting != 1 && welcomeMsg) {
                            // console.log("messageData: ",messageData)
                            conversations[messageData.room].emitMessage({
                                msg: welcomeMsg
                            });
                        } else {
                            conversations[messageData.room].emitOpTyping({room: messageData.room});
                            conversations[messageData.room].sendMessagesFromConsole(messageData);
                        }
                    })
                } else {
                    conversations[messageData.room].emitOpTyping({room: messageData.room});
                    conversations[messageData.room].sendMessagesFromConsole(messageData);
                }
            })
        }
    });
    socketioClient.removeListener('_twitterAccounts_').on('_twitterAccounts_', function (data) {
        if (data.twitterAccounts instanceof Array && data.twitterAccounts.length > 0) {
            async.eachSeries(data.twitterAccounts, function (account, callback) {
                runTwitterInstance(account, callback);
            }, function (err) {
                if (err) {
                    process.emit('asyncLogging', {type: 'logging', parameter: err.stack, prefix: PREFIX});
                }
            });
        }
    });
    // #region edited area
    socketioClient.removeListener('customerDisconnect').on('customerDisconnect', function (data) {
        if (conversations[data.room]) {
            try {
                conversations[data.room].customerDisconnected();
            } catch (err) {
                process.emit('asyncLogging', {type: 'customerDisconnected', parameter: err.stack || err, prefix: PREFIX});
            }
        }
    });

    socketioClient.removeListener('customerReconnected').on('customerReconnected', function (data) {
        if (conversations[data.room]) {
            try {
                conversations[data.room].customerReconnected();
            } catch (err) {
                process.emit('asyncLogging', {type: 'customerReconnected', parameter: err.stack || err, prefix: PREFIX});
            }
        }
    });
    // #endregion
    socketioClient.on('close', function (e, d) {
        console.log('socketioClient|closed');
        console.log('e:', e);
        console.log('d:', d);
    });
}
var directLineUserId = process.env.DIRECT_LINE_USER_ID ? process.env.DIRECT_LINE_USER_ID : 'Bubbles-CC-BOT';
// var directLineUserId = 'linkdevfbbottest_Bot';
/*var directLineClient;
 rp(directLineSpecUrl) //, {agent: keepaliveAgent}
 .then(function (spec) {
 return new Swagger({
 spec: JSON.parse(spec.trim()),
 client: myAgent,
 usePromise: true
 });
 })
 .then(function (client) {
 client.clientAuthorizations.add('AuthorizationBotConnector', new Swagger.ApiKeyAuthorization('Authorization', 'Bearer ' + directLineSecret, 'header'));
 //                console.log("typeof client:", typeof client);
 directLineClient = client;
 })
 .catch(function (err) {
 process.emit('asyncLogging', {type: 'client.clientAuthorizations', parameter: err.stack, prefix: PREFIX});
 });*/

function BubblesBot(data) {
    /*data: { room: '59fee660ecb19e7c938852af',thirdPartyCustomers: 'whatsapp'| 'twitter' }*/
    var self = this;
    var welcome_ar = 'مرحبا';
    var welcome_en = 'hello';
    var startConversationPath = '/v3/directline/conversations';
    let postActivityPath = function (conversationId) {
        return `/v3/directline/conversations/${conversationId}/activities`;
    };
    this.botAgentId = data.botAgentId;
    // var startConversationMsg = 'start_new_conversation'
    this.config = {
        appointment: false,
        greeting: true,
        shopcart: false,
        faq: true,
        defaultDialog: 'faq',
        openingDialog: ''
    }
    this.customerId = data.customerId;
    this.nick = data.nick;
    this.email = data.email;
    this.prefix = data.prefix;
    this.dept = data.dept;
    this.room = data.room;
    this.lang = data.lang;
    this.ip = data.ip;
    this.timer;
    this.apiKey = APIKEY;
    this.thirdPartyCustomers = data.thirdPartyCustomers;
    this.twitterTimeout = null;
    this.agreement = data.agreement
    this.startTime = new Date().getTime()
    if (data.mobile) {
        this.phone = data.mobile;
    }
    if(data.firstMsg){
        this.firstMsg = data.firstMsg;
    }
    roomsArr.push(data.room);
    //StartConversation -- directLineClient.Conversations.Conversations_StartConversation
    _postDirectLineRequest({path: startConversationPath})
            .then(function (response) {
                response = JSON.parse(response);
                self.responseObj = response;
                self.startReceivingWebSocketClient();
            })
            .catch(function (err) {
                process.emit('asyncLogging', {type: 'client.Conversations.Conversations_StartConversation', parameter: err.stack || err, prefix: PREFIX});
            });
    BubblesBot.prototype.sendMessagesFromConsole = function (messageData) {
        // console.log(">>>>>>>>>>sendMessagesFromConsole", messageData.msg);
        var self = this;
        var activityData = {
            textFormat: 'plain',
            text: messageData.msg,
            type: 'message',
            textLocale: this.lang,
            from: {
                id: this.customerId ? this.customerId : this.startTime + '_' + directLineUserId,
                botAgentId: this.botAgentId,
                name: this.nick,
                email: this.email,
                prefix: this.prefix,
                dept: this.dept,
                room: this.room,
                ip: this.ip,
                apiKey: this.apiKey,
                configuration: this.config,
                thirdPartyCustomers: this.thirdPartyCustomers,
                agreement: this.agreement
            }
        };
        if (messageData.phone || this.phone) {
            activityData.from.phone = messageData.phone || this.phone;
        }

        if (messageData.systemNotification === true) {
            return false;
        }

        if (messageData.seenData) {
            activityData.seenData = messageData.seenData;
            clearTimeout(self.twitterTimeout);
            self.twitterTimeout = null;
        }

        if (this.thirdPartyCustomers === 'twitter') {
            var twitterScreenNameRegex = /(^|[^@\w])@(\w{1,15})\b/g;
            activityData.text = activityData.text.replace(twitterScreenNameRegex, '');
        }

        if (messageData.hideChat) {
            // console.log("messageData.hideChat",messageData);
            return this.distroyConversation(messageData.room);
        }
        if (messageData.transferToDep) {
            activityData.from.transferToDep = true;
        }


        //PostActivity  -- directLineClient.Conversations.Conversations_PostActivity
        return _postDirectLineRequest({
            path: postActivityPath(this.responseObj.conversationId),
            body: activityData
        })
                .then(function (body) {
                    body = JSON.parse(body);
//                    process.emit('asyncLogging', {type: 'client.Conversations.Conversations_PostActivity.body', parameter: body});
                    activityData = null;
                    return false;
                })
                .catch(function (err) {
//                    if (err.data) { // && err.data.message === "Failed to send activity: bot timed out"
//                        return false;
//                    }
                    process.emit('asyncLogging', {type: 'Error sending message', parameter: err.data || err.errObj || err, prefix: PREFIX}); // + require('util').inspect(err, {depth: null})
                    return false
                    return self.emitMessage({
                        msg: "Sorry..we can't send your message, please try again",
                        systemNotification: true,
                        alertType: 'er'
                    });
                });
//            _HandlePostActivity(this.client, this.responseObj.conversationId, activityData);

        /*updateMsgCount(function (count) {
         //ToDo: enabled for retest
         // if(count < msglimitPerPackage && dayMsgsCount < msglimitPerDay){
         //                if (self.client) {
         //                    return _HandlePostActivity(self.client, self.responseObj.conversationId, activityData);
         //                }
         //                } else {
         //                    self.emitMessage({msg: "Sorry..we can't send your message.", systemNotification: true, alertType: 'er'});
         //                    process.emit('asyncLogging', {type: 'Error sending message', parameter: 'Sorry..we can not send your message, please try again' + err, prefix: PREFIX});
         //                }
         })*/
    }

    BubblesBot.prototype.startReceivingWebSocketClient = function () {
        var self = this;
        this.ws = new (require('websocket').client)();
        this.ws.on('connectFailed', function (error) {
            process.emit('asyncLogging', {type: 'Connect Error', parameter: error.toString(), prefix: PREFIX});
//            console.log('Connect Error: ' + error.toString());
        });
        this.ws.on('connect', function (connection) {
            self.ws_connection = connection;
            connection.on('error', function (error) {
                process.emit('asyncLogging', {type: 'WS Error', parameter: error.toString(), prefix: PREFIX});
            });
            connection.on('close', function () {
//                process.emit('asyncLogging', {type: 'WS connection', parameter: 'WS connection closed', prefix: PREFIX});
//                console.log('WebSocket Client Disconnected');
            });
            connection.on('message', function (message) {
                if (message.type === 'utf8' && message.utf8Data.length > 0) {
                    var data = JSON.parse(message.utf8Data);
                    if (data.activities[0].from.id === (self.customerId ? self.customerId : self.startTime + '_' + directLineUserId) ) {
                        if (data.activities[0].seenData) {
                            socketioClient.emit('seen', {prefix: PREFIX, seenData: data.activities[0].seenData});
                        }
                        return false;
                    }
                    self.emitOpStoptyping({room: self.room});
                    switch (data.activities[0].text) {
                        case 'emitEndSession':
                            return self.emitEndSession();
                            break;
                        case 'emitGetOnlineDepts':
                            return self.emitGetOnlineDepts(data.activities[0].channelData.onlineDeptsRequestData);
                            break;
                        case 'emitOpTransferSession':
                            return self.emitOpTransferSession(data.activities[0].channelData.transferSessionData);
                            break;
                        case 'emitUpdateSessionCustomerData':
                            return self.emitUpdateSessionCustomerData(data.activities[0].channelData);
                            break;
                        case 'emitOpTyping':
                            return self.emitOpTyping({room: self.room});
                        case 'emitNewOrderCreated':
                            return self.emitNewOrderCreated(data.activities[0].channelData.orderData);
                            break;
                        default:
                            if (data.activities[0].text === 'emitEndSession') {
                                return false;
                            }
                            var messageData = {msg: data.activities[0].text};
                            if (data.activities[0].channelData) {
                                messageData['systemNotification'] = data.activities[0].channelData.systemNotification;
                                messageData['alertType'] = data.activities[0].channelData.alertType;
                                messageData['textAlign'] = self.lang === 'ar' ? 'right' : 'left';
                                messageData['imageCard'] = data.activities[0].channelData.imageCard;
                                messageData['optionCard'] = data.activities[0].channelData.optionCard;
                                messageData['cards'] = data.activities[0].channelData.cards;
                            }
                            return self.emitMessage(messageData);
                            //ToDo: enable for retest
                            /*if (process.env.NODE_ENV === 'production') {
                             self.emitMessage(messageData);
                             } else {
                             updateMsgCount(function (count) {
                             if (count < msglimitPerPackage && dayMsgsCount < msglimitPerDay) {
                             self.emitMessage(messageData);
                             } else {
                             self.emitMessage({msg: "Sorry..we can't send your message.", systemNotification: true, alertType: 'er'});
                             }
                             })
                             }*/
                    }
                }
            });
            socketioClient.emit('operatorJoinRoom', {room: self.room, prefix: PREFIX, nickname: 'BanqueMisr Smart Bot', nicknameAr: 'المساعد الذكي'}, function () {
                if (self.thirdPartyCustomers !== 'twitter' && self.thirdPartyCustomers !== 'whatsapp' && !self.firstMsg) {
                    getCachedRedisSessionConfig(function (sessionConfig) {
                        // self.emitOpTyping({room: self.room});
                        // process.sessionConfig = sessionConfig;
                        if (process.env.NODE_ENV === 'production') {
                            self.emitOpTyping({room: self.room});
                            var welcomeMessage = self.lang == 'ar' ? sessionConfig.startBot_question_ar : sessionConfig.startBot_question;
                            self.sendMessagesFromConsole({
                                msg: welcomeMessage ? welcomeMessage : self.lang == 'ar' ? welcome_ar : welcome_en //'Tour'//self.lang == 'ar' ? welcome_ar : welcome_en
                            });
                        } else {
                            var welcomeMsg = self.lang === 'en' ? sessionConfig.welcomeBot_msg : sessionConfig.welcomeBot_msgAr;
                            if(sessionConfig.enableDialogWithGreeting != 1 && welcomeMsg){
                                self.emitMessage({
                                    msg: welcomeMsg
                                });
                            }else{
                                self.emitOpTyping({room: self.room});
                                self.sendMessagesFromConsole({
                                    msg: self.lang === 'en' ? welcome_en : welcome_ar
                                });
                            }
                            /*
                            var startquestion = self.lang == 'ar' ? sessionConfig.startBot_question_ar : sessionConfig.startBot_question;
                            console.log('startquestion:', startquestion)
                            if (startquestion) {
                                self.emitOpTyping({room: self.room});
                                self.sendMessagesFromConsole({
                                    msg: startquestion
                                });
                            } else {
                                var welcomeMsg = self.lang === 'en' ? sessionConfig.welcomeBot_msg : sessionConfig.welcomeBot_msgAr
                                if (welcomeMsg) {
                                    self.emitMessage({
                                        msg: welcomeMsg
                                    });
                                } else {
                                    self.emitOpTyping({room: self.room});
                                    self.sendMessagesFromConsole({
                                        msg: self.lang === 'en' ? welcome_en : welcome_ar
                                    })
                                }
                            }
                            */
                        }
                    })
                }
            });
        });
        this.ws.connect(self.responseObj.streamUrl);
    };
    BubblesBot.prototype.emitOpTransferSession = function (transferSessionData) {
        var self = this;
    //    console.log('transferSessionData:', transferSessionData);
        socketioClient.emit('opTransferSession', {
            target: transferSessionData.onlineDeptId ? 1 : 0, //0 >>>> toSupervisor; 1 >>> toOnlineDept
            id: transferSessionData.onlineDeptId ? transferSessionData.onlineDeptId : transferSessionData.supervisorId,
            room: self.room,
            userId: self.botAgentId,
            prefix: PREFIX //getServerPrefix()
        }, function () {
            // console.log('>>opTransferSession|CallBack');
//            self.client = null;
//            delete self.client;
            if (conversations[self.room]) {
                conversations[self.room].ws = null;
                delete conversations[self.room].ws;
                conversations[self.room] = null;
                delete conversations[self.room];
            }
        });
    };
    BubblesBot.prototype.emitGetOnlineDepts = function (onlineDeptsRequestData) {
        var self = this;
        socketioClient.emit('getOnlineDepts', onlineDeptsRequestData, function (onlineDeptsResponseData) {
            try {
                var obj = typeof JSON.parse(onlineDeptsResponseData) === "object" ? JSON.parse(onlineDeptsResponseData) : null;
            } catch (err) {
                return self.sendMessagesFromConsole({msg: 'null'});
            }
            try {
                self.sendMessagesFromConsole({msg: obj[1][0]._id, transferToDep: true});
            } catch (e) {
                self.sendMessagesFromConsole({msg: obj[0][0]._id});
            }
        });
    };
    BubblesBot.prototype.emitEndSession = function () {
//        console.log(">>>>>>>>>emitEndSession");
        var self = this;
        var leaveRoomData = {
            how: 'operator',
            room: self.room,
            nick: directLineUserId,
            timeout: false,
            disconnectType: false,
            prefix: PREFIX,
            disconnect: true
        };
        var x = setTimeout(function () {
            socketioClient.emit('leaveRoom', leaveRoomData, function (e) {
                // console.log("leaveRoom")
                self.distroyConversation(self.room,self.thirdPartyCustomers === 'twitter' ? false : true);
            });
        }, 1000 * 3);
    };
    BubblesBot.prototype.emitMessage = function (data) {
        // console.log("emitMessage|data.msg:", data.msg);
        var self = this;
        var messageData = {
            msg: data.msg,
            nick: directLineUserId,
            who: 'op',
            room: self.room,
            avatar: process.env.AVATAR || 'https://' + DomainName + ':443/upload/avatars/' + PREFIX + '/avatar.png',
            prefix: PREFIX,
            msgId: new Date().getTime()
        };
        if (data.systemNotification === true) {
            messageData.systemNotification = true;
        }

        if (data.alertType) {
            messageData.alertType = data.alertType;
        }

        if (data.textAlign) {
            messageData.textAlign = data.textAlign;
        }

        if (data.imageCard) {
            messageData.imageCard = true;
            messageData.sendCardMessage = true;
        }

        if (data.optionCard) {
            messageData.optionCard = true;
            messageData.sendCardMessage = true;
        }

        if (data.cards) {
            messageData.cards = data.cards;
        }

        socketioClient.emit("message", messageData, function (messageRes) {
//            if (messageRes.err === 1) {
//                console.log('messageRes.err == 1');
//            }

            if (self.thirdPartyCustomers === 'twitter' && self.twitterTimeout === null) {
                getCachedRedisSessionConfig(function (sessionConfig) {
                    var waitingMin = 20;
                    if(sessionConfig.waitingUserResponseMinutes){
                        waitingMin = parseInt(sessionConfig.waitingUserResponseMinutes)
                    }
                    self.twitterTimeout = setTimeout(function () {
                        self.emitEndSession();
                    }, 1000 * 60 * waitingMin);    
                })
            }
        });
    };
    BubblesBot.prototype.emitUpdateSessionCustomerData = function (data) {
        this.nick = data.name;
        this.phone = data.mobile;
    };
    BubblesBot.prototype.distroyConversation = function (room, botClosedSession) {
        var self = this;
        // console.log('>>distroyConversation');
        if (!botClosedSession) {
            clearBotData(function () {
                clearConnectorDate();
            })
        } else {
            clearConnectorDate();
        }

        function clearBotData(calback) {
            self.sendMessagesFromConsole({msg: 'clear_conversation_data'});
            return calback();
        }
        function clearConnectorDate() {
            var index = roomsArr.indexOf(room);
            if (index > -1) {
                roomsArr.splice(index, 1);
            }
            if (conversations[room]) {
                try {
                    conversations[room].ws_connection.close();
                } catch (err) {
                    process.emit('asyncLogging', {type: 'distroyConversation', parameter: err.stack || err, prefix: PREFIX});
                }
                conversations[room].ws = null;
                delete conversations[room].ws;
                conversations[room] = null;
                delete conversations[room];
            }
        }
    };
    BubblesBot.prototype.emitOpTyping = function (data) {
        // console.log('emitOpTyping|data:', require('util').inspect(data));
        return socketioClient.emit("opTyping", data);
    };
    BubblesBot.prototype.emitOpStoptyping = function (data) {
        // console.log('emitOpStoptyping|data:', require('util').inspect(data));
        return socketioClient.emit("opStoptyping", data);
    };
    BubblesBot.prototype.emitNewOrderCreated = function(data){
        console.log('emitNewOrderCreated|Data', data)
        socketioClient.emit('newOrderCreated', data);
    }
// #region edited area
    BubblesBot.prototype.customerDisconnected = function () {
        var self = this;
        // console.log("customerDisconnected")
        self.distroyConversation(self.room);
        clearTimeout(self.timer);

        /*self.timer = setTimeout(function () {
         var leaveRoomData = {
         how: 'customer',
         room: self.room,
         nick: self.nick,
         timeout: true,
         disconnectType: false,
         prefix: PREFIX,
         disconnect: true
         };
         socketioClient.emit('leaveRoom', leaveRoomData, function (e) {
         self.distroyConversation(self.room);
         clearTimeout(self.timer);
         });
         }, (1000 * 50));*/
    }
    BubblesBot.prototype.customerReconnected = function () {
        var self = this;
        clearTimeout(self.timer);
    }
// #endregion
}

function handleBorReconnected() {
    async.eachSeries(roomsArr,
            function (room, callback) {
                socketioClient.emit('reconnected', {
                    room: room
                }, function (reconnectedResponse) {
                    if (reconnectedResponse.err == 1 && conversations[room]) {
                        // console.log("reconnectedResponse.err ")
                        conversations[room].distroyConversation(room);
                    }
                    callback();
                });
            }, function (err) {
        if (err) {
            console.log('handleBorReconnected|err:', err);
        }
    });
}

function updateMsgCount(calback) {
    var options = {
        url: PROTOCOL + '://' + HOST + ':' + PORT + '/' + PREFIX + '/api/updateTotalMessagesCount',
        method: 'POST',
        headers: {
            'tokenKey': process.sessionConfig.tokenKey
        }
    }
    request(options, function (error, response, body) {
        // console.log('updateMsgCount:', body);
        var data;
        try {
            data = JSON.parse(body)
        } catch (e) {
            data = body
        }
        if (!error && data.err == '0') {
            dayMsgsCount += 1;
            currentMsgsCount = data.count;
            if (typeof calback === 'function')
                calback(currentMsgsCount);
            return false;
        } else if (data.tokenKeyErr) {
            return  handleTokenKeyExpired(updateMsgCount, calback);
        } else {
            process.emit('asyncLogging', {type: 'logging', prefix: PREFIX, method: 'updateMsgCount|request|error', parameter: error, options: options, response: response, prefix: PREFIX});
            if (typeof calback === 'function')
                calback(currentMsgsCount);
            return false;
        }
    })
}

function getCurrentMsgCount() {
    var options = {
        url: PROTOCOL + '://' + HOST + ':' + PORT + '/' + PREFIX + '/api/getCurrentMsgCount',
        method: 'POST',
        headers: {
            'tokenKey': process.sessionConfig.tokenKey
        }
    }

    request(options, function (error, response, body) {
        if (!error && body.err == '0') {
            currentMsgsCount = body.count;
            return false;
        } else if (body.tokenKeyErr) {
            return handleTokenKeyExpired(getCurrentMsgCount, calback);
        } else {
            process.emit('asyncLogging', {type: 'logging', prefix: PREFIX, method: 'getCurrentMsgCount|request|error', parameter: error.stack, options: options, response: response, prefix: PREFIX});
            return false;
        }
    })

}

/*var day = setInterval(function () {
 console.log('>>>>>>>>>>>clearDayCount')
 dayMsgsCount = 0
 }, 1000 * 60 * 60 * 24);*/

function handleTokenKeyExpired(fn, parm) {
    return new Promise((resolve, reject) => {
        requestSessionConfig((sessionConfig) => {
            resolve(fn(parm));
        })
    })
}


function _postDirectLineRequest(requestData) {
    return new Promise(function (resolve, reject) {
        let _directLineRes = '';
        const data = requestData.body ? JSON.stringify(requestData.body) : '{}';
        const headers = {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Content-Length': Buffer.byteLength(data),
            Authorization: 'Bearer ' + directLineSecret
        };
        const options = {
            hostname: 'directline.botframework.com',
            path: requestData.path,
            method: requestData.method || 'POST',
            headers: headers,
            agent: keepaliveAgent,
        };
//        console.log('_postDirectLineRequest|headers:', options.headers);
//        console.log('_postDirectLineRequest|data:', data);

        const req = https.request(options, res => {
//            console.log(`statusCode: ${res.statusCode}`);
            res.setEncoding('utf8');
            res.on('data', chunk => {
                _directLineRes += chunk;
//                resolve.bind(_directLineRes);
            });
            res.on('end', function () {
//                console.log('_directLineRes:', _directLineRes);
                if (res.statusCode > 201) {
                    process.emit('asyncLogging', {type: 'logging', prefix: PREFIX, method: '_postDirectLineRequest|request|statusCode', parameter: 'statusCode:' + res.statusCode, prefix: PREFIX});
//                    _directLineRes.statusCode = res.statusCode;
                    return reject(_directLineRes);
                } else {
                    resolve(_directLineRes);
                }
            });
            res.on('error', reject);
        });
        req.on('error', reject);
        req.write(data)
        req.end()
    });
}

//UnUsed
function _startConversation() {
    let _startConversationData = {
        path: '/v3/directline/conversations'
    };
    return _postDirectLineRequest(_startConversationData);
}
function _postActivity(activityData) {
    //activityData: {conversationId, activityBody}

    let _postActivityData = {
        path: '/v3/directline/conversations/' + activityData.conversationId + '/activities',
        body: activityData.activityBody
    }
    postActivityPath = function (conversationId) {
        return `/v3/directline/conversations/${conversationId}'/activities`;
    }
    return _postDirectLineRequest(_postActivityData);
}
function _HandlePostActivity(client, conversationId, activityData) {
    _PostActivity(client, conversationId, activityData)
            .then(function (body) {
//                process.emit('asyncLogging', {type: 'client.Conversations.Conversations_PostActivity.body', parameter: body});
                activityData = null;
                return false;
            })
            .catch(function (err) {
//                process.emit('asyncLogging', {type: 'client.Conversations.Conversations_PostActivity', parameter: err.stack || err});
                process.emit('asyncLogging', {type: 'Error sending message', parameter: err.data || err.errObj || err, prefix: PREFIX}); // + require('util').inspect(err, {depth: null})

//                setTimeout(function () {
//                    return _HandlePostActivity(client, conversationId, activityData);
//                }, 1000 * 2);

                //ToDo: implement email notification
            });
}
/*function _PostActivity(client, conversationId, data) {
 return new Promise(function (resolve, reject) {
 client.Conversations.Conversations_PostActivity({
 conversationId: conversationId,
 activity: data
 })
 .then(resolve)
 .catch(reject);
 });
 }*/
