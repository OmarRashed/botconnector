
module.exports = function (data) {
//data:{type,client,event,paramerter}
    switch (data.type) {
        case 'uncaughtException':
            handleUncaughtException(data);
            break;
        default:
            asyncLogging(data);
    }
};

function getCurrentDate() {
    var strftime = require('strftime');
    return strftime('%d%m%Y');
}
function asyncLogging(data) {
    var currentDate = getCurrentDate();
    var socketioLogFile = currentDate + '.log';
    var logs = data;
            /*{
        event: data.event,
        prefix: data.client.prefix,
        webChat: data.client.webChat,
        userAgent: data.client.handshake.userAgent,
        parameter: data.parameter
    }*/
    
    if (data.err) {
        logs['error'] = data.err.message;
    }
    
    return appendFile(socketioLogFile, logs);
}
function handleUncaughtException(data){
    var currentDate = getCurrentDate();
    var logFile = 'uncaughtException' + currentDate + '.log';
    var logs = data;
    
    return appendFile(logFile, logs);
}
function appendFile(logFile, logs) {
    var fs = require("fs");
    var path = require('path');
    var mkdirp = require('mkdirp');
    var fileDirectory = path.join(__dirname, '../logs/'+logs.prefix + '/');
    delete logs.prefix;
    mkdirp(fileDirectory,function(error){
        if(error){
            console.log("error:", error);
        }else{          
            fs.appendFile(path.join(fileDirectory + logFile), '[' + new Date() + ']:\r\n' + require('util').inspect(logs) + '\r\n----------\r\n',
            function (error) {
                if (error) {
                    console.log("written file");
                }
            });
        }
    })
    return false;
}