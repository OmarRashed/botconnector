//"use strict";

var async = require('async');
var redisOptions = {
    parser: 'javascript'
};

var port = 6379;
// var host =  'localhost'; //'gigny1.cloudapp.net';//process.env.NODE_ENV === 'dev_production' ? '10.0.0.8' :
var host =  'staging.adsia.com'; //'gigny1.cloudapp.net';//process.env.NODE_ENV === 'dev_production' ? '10.0.0.8' : 137.117.136.63
var pass = 'a6ca5e85e6853db7dba5dcb012e5b9b5';
var redis = require('redis');
redis.RedisClient.prototype.delWildcard = function (key, callback) {
    var redis = this;

    redis.keys(key, function (err, rows) {
        async.each(rows, function (row, callbackDelete) {
            redis.del(row, callbackDelete);
        }, callback);
    });
};

exports.redis = redis;
if (process.env.SOCKETIO === 'stand-alone' || process.env.NODE_ENV === 'localhost') {
    var pub = redis.createClient(port, host, redisOptions);

    pub.auth(pass, function (err, res) {
    });

    pub.on('error', function (e) {
        console.log('subscriber', e.stack);
        process.exit(1);
    });

    exports.pub = pub;

    var sub = redis.createClient(port, host, redisOptions);

    sub.auth(pass, function () {
    });

    sub.on('error', function (e) {
        console.log('publisher', e.stack);
        process.exit(1);
    });

    exports.sub = sub;
}

var socketStore = redis.createClient(port, host, redisOptions);

socketStore.auth(pass, function () {
});

socketStore.on('error', function (e) {
    console.log('socketStore', e.stack);
    process.exit(1);
});

exports.socketStore = socketStore;